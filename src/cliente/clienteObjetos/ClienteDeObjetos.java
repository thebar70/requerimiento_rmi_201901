/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.clienteObjetos;

import java.rmi.RemoteException;
import servidor.sop_rmi.GestionUsuariosInt;
import cliente.utilidades.UtilidadesConsola;
import java.time.LocalDate;
import java.util.ArrayList;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import servidor.sop_rmi.GestionAnteproyectosInt;

/**
 *
 * @author user
 */
public class ClienteDeObjetos {
    private static GestionUsuariosInt objRemotoUser;
    private static GestionAnteproyectosInt objRemotoAP;
    public static void main(String[] args) throws RemoteException
    {
        int numPuertoRMIRegistry;
        String direccionIpRMIRegistry;        

        //System.out.println("Cual es la direccion ip donde se encuentra  el rmiregistry ");
        direccionIpRMIRegistry = "localhost";
        //System.out.println("Cual es el numero de puerto por el cual escucha el rmiregistry ");
        numPuertoRMIRegistry = 2020; 

        objRemotoUser = (GestionUsuariosInt) cliente.utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry,numPuertoRMIRegistry, "ObjetoRemotoUsuarios");
        objRemotoAP = (GestionAnteproyectosInt) cliente.utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry,numPuertoRMIRegistry, "ObjetoRemotoAnteproyectos");
        
        MenuPrincipal();
    }
    private static void MenuPrincipal() throws RemoteException
    {
        System.out.println("Ingrese usuario");
        String estado = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese contrasena");
        String contrasena = UtilidadesConsola.leerCadena();
        IniciarSesionDTO objIS = new IniciarSesionDTO(estado, contrasena);
        System.out.println(objRemotoUser.iniciarSesion(objIS));
        
        if( objRemotoUser.iniciarSesion(objIS) == 1)
        {
            int opcion = 0;
            do
            {
                System.out.println("==Menu==");
                System.out.println("1. Registrar AnteProyecto");			
                System.out.println("2. Registrar Usuarios");
                System.out.println("3. buscar AP");
                System.out.println("4. Listar AP");
                System.out.println("5. Asginar Evaluador AP");
                System.out.println("6. Salir");

                opcion = UtilidadesConsola.leerEntero();

                switch(opcion)
                {
                    case 1:
                        Opcion1();
                    break;
                    case 2:
                        Opcion2();
                    break;
                    case 3:
                        Opcion3();
                    break;
                    case 4:
                        Opcion4();
                    break;
                    case 5:
                        //Opcion5();
                    break;
                  
                    default:
                        System.out.println("Opción incorrecta");
                }
            }while(opcion != 5);
        }
    }
    
    private static void Opcion1() throws RemoteException 
    {
        System.out.println("==Registro AP==");
        System.out.println("Ingrese modalidad AP: ");
        String modalidad = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese titulo AP: ");
        String titulo = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese el Codigo ");
        int codigo = UtilidadesConsola.leerEntero();
        System.out.println("Ingrese Estudainte 1");
        String estudiante_1 = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese Estudainte 2");
        String estudiante_2 = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese Director");
        String director = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese CoDirector");
        String codirector = UtilidadesConsola.leerCadena();
        
        AnteproyectoDTO objAP = new AnteproyectoDTO(modalidad, titulo, codigo, estudiante_1, estudiante_2, director, codirector);
        objRemotoAP.registrarAnteproyectos(objAP);
    }
    
    private static void Opcion2() throws RemoteException 
    {
        System.out.println("==Registro Usuarios==");
        System.out.println("Ingrese Nombre Usuario: ");
        String titulo = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese el ID ");
        int estudiante = UtilidadesConsola.leerEntero();
        System.out.println("Ingrese usuario");
        String estado = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese contrasena");
        String contrasena = UtilidadesConsola.leerCadena();
        System.out.println("Ingrese el tipo");
        int tipo = UtilidadesConsola.leerEntero();

        UsuarioDTO obj = new UsuarioDTO(titulo, estudiante, estado, contrasena, tipo);
        objRemotoUser.registrarUsuario(obj);
    }
    private static void Opcion3() throws RemoteException 
    {
        System.out.println("==Buscar AP==");
        System.out.println("Ingrese Codigo AP: ");
        int codigoAp = UtilidadesConsola.leerEntero();
        
        AnteproyectoDTO objAP = objRemotoAP.buscarAnteproyecto(codigoAp);
        
        System.out.println("Modalidad: "+objAP.getModalidad());
        System.out.println("Titulo: "+objAP.getTitulo());
        System.out.println("Codigo Ap: "+objAP.getCodigoAP());
        System.out.println("Estudainte 1: "+objAP.getEstudiente_1());
        System.out.println("Estudiante 2: "+objAP.getEstudainte_2());
        System.out.println("Director: "+objAP.getDirector());
        System.out.println("Codirector: "+objAP.getCoDirector());
        System.out.println("Fecha Registro: "+objAP.getFechaRegistro());
        System.out.println("Fecha Aprobacion: "+objAP.getFechaAprobacion());
        System.out.println("Concepto: "+objAP.getConcepto());
        System.out.println("Estado: "+objAP.getEstado());
        System.out.println("Revision: "+objAP.getNumRevision());
    }
    
    private static void Opcion4() throws RemoteException 
    {
        System.out.println("==Listar AP==");
        System.out.println("Ingrese Codigo AP: ");
        int codigoAp = UtilidadesConsola.leerEntero();
        
        ArrayList<AnteproyectoDTO> objAP = objRemotoAP.listarAnteproyectos();
        
        for(int i = 0; i < objAP.size(); i++)
        {
            System.out.println("Modalidad: "+objAP.get(i).getModalidad());
            System.out.println("Titulo: "+objAP.get(i).getTitulo());
            System.out.println("Codigo Ap: "+objAP.get(i).getCodigoAP());
            System.out.println("Estudainte 1: "+objAP.get(i).getEstudiente_1());
            System.out.println("Estudiante 2: "+objAP.get(i).getEstudainte_2());
            System.out.println("Director: "+objAP.get(i).getDirector());
            System.out.println("Codirector: "+objAP.get(i).getCoDirector());
            System.out.println("Fecha Registro: "+objAP.get(i).getFechaRegistro());
            System.out.println("Fecha Aprobacion: "+objAP.get(i).getFechaAprobacion());
            System.out.println("Concepto: "+objAP.get(i).getConcepto());
            System.out.println("Estado: "+objAP.get(i).getEstado());
            System.out.println("Revision: "+objAP.get(i).getNumRevision());
            System.out.println("-------------------------------------------------");
        }
        
    }
}
