/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.vistas.VistasJefe;

import cliente.vistas.Login.frmInicisoSesion;
import java.awt.Dimension;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class frmVistaPrincialJefe extends javax.swing.JFrame {

    /**
     * Creates new form frmVistaPrincial
     */
    public frmVistaPrincialJefe() {
        initComponents();
        this.setPreferredSize(new Dimension(200, 600));
        setTitle("GESTOR ANTEPROYECTOS 2.0");
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        imgJefe = new javax.swing.JLabel();
        btnRegUser = new javax.swing.JButton();
        lblRegUser = new javax.swing.JLabel();
        bntAsgEval = new javax.swing.JButton();
        lblRegAP = new javax.swing.JLabel();
        bntRegAP1 = new javax.swing.JButton();
        lbAsgEval = new javax.swing.JLabel();
        bntBuscarAp = new javax.swing.JButton();
        lbBuscar = new javax.swing.JLabel();
        bntListarAp = new javax.swing.JButton();
        lbListar = new javax.swing.JLabel();
        jSlide = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelPrincipal.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelPrincipal.setPreferredSize(new java.awt.Dimension(850, 650));
        jPanelPrincipal.setVerifyInputWhenFocusTarget(false);
        jPanelPrincipal.setLayout(null);

        imgJefe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/jefe.png"))); // NOI18N
        imgJefe.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "JEFE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Copperplate Gothic Bold", 0, 14))); // NOI18N
        jPanelPrincipal.add(imgJefe);
        imgJefe.setBounds(20, 110, 154, 163);

        btnRegUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/regUser.png"))); // NOI18N
        jPanelPrincipal.add(btnRegUser);
        btnRegUser.setBounds(60, 280, 80, 40);

        lblRegUser.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblRegUser.setText("RegistrarUsuario");
        jPanelPrincipal.add(lblRegUser);
        lblRegUser.setBounds(50, 320, 97, 24);

        bntAsgEval.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/asgEval.png"))); // NOI18N
        jPanelPrincipal.add(bntAsgEval);
        bntAsgEval.setBounds(60, 420, 78, 40);

        lblRegAP.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblRegAP.setText("RegistrarAnteProyecto");
        jPanelPrincipal.add(lblRegAP);
        lblRegAP.setBounds(30, 390, 130, 24);

        bntRegAP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/regAP.png"))); // NOI18N
        jPanelPrincipal.add(bntRegAP1);
        bntRegAP1.setBounds(60, 350, 78, 40);

        lbAsgEval.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lbAsgEval.setText("Asignar Evaluadores");
        jPanelPrincipal.add(lbAsgEval);
        lbAsgEval.setBounds(40, 460, 117, 24);

        bntBuscarAp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/busqueda.png"))); // NOI18N
        jPanelPrincipal.add(bntBuscarAp);
        bntBuscarAp.setBounds(60, 490, 78, 40);

        lbBuscar.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lbBuscar.setText("BuscarAnteproyecto");
        jPanelPrincipal.add(lbBuscar);
        lbBuscar.setBounds(40, 530, 117, 24);

        bntListarAp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/listar.png"))); // NOI18N
        jPanelPrincipal.add(bntListarAp);
        bntListarAp.setBounds(60, 560, 78, 40);

        lbListar.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lbListar.setText("Listar Anteproyectos");
        jPanelPrincipal.add(lbListar);
        lbListar.setBounds(40, 600, 119, 24);

        jSlide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/slide.png"))); // NOI18N
        jPanelPrincipal.add(jSlide);
        jSlide.setBounds(0, 0, 870, 110);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Cliente/Vistas/Img/cerrarSesion.png"))); // NOI18N
        jButton1.setText("Cerrar Sesion");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanelPrincipal.add(jButton1);
        jButton1.setBounds(710, 120, 120, 25);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Salio " , " ", JOptionPane.INFORMATION_MESSAGE);
        this.dispose();
        //new frmInicisoSesion().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmVistaPrincialJefe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmVistaPrincialJefe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmVistaPrincialJefe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmVistaPrincialJefe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmVistaPrincialJefe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntAsgEval;
    private javax.swing.JButton bntBuscarAp;
    private javax.swing.JButton bntListarAp;
    private javax.swing.JButton bntRegAP1;
    private javax.swing.JButton btnRegUser;
    private javax.swing.JLabel imgJefe;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanelPrincipal;
    private javax.swing.JLabel jSlide;
    private javax.swing.JLabel lbAsgEval;
    private javax.swing.JLabel lbBuscar;
    private javax.swing.JLabel lbListar;
    private javax.swing.JLabel lblRegAP;
    private javax.swing.JLabel lblRegUser;
    // End of variables declaration//GEN-END:variables
}
