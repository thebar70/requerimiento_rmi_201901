/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;
/**
 *
 * @author user
 */
public class AnteproyectoDAO { 
    
    public boolean GuardarDatosAP(AnteproyectoDTO objAnteproyecto)
    {
        boolean respuesta = false;
        File fichero = null;
        FileOutputStream fos;
        ObjectOutputStream ois;
        
        try
        {
            fichero = new File("anteproyectos.txt");
            fos = new FileOutputStream(fichero);
            ois = new ObjectOutputStream(fos);
            ois.writeObject(objAnteproyecto);
            ois.close();
            respuesta = true;
        }
        catch(FileNotFoundException e)
        {
            System.out.println(" AnteproyectoDAO: Error en la localizacion del archivo anteproyectos.txt");
            respuesta = false;
        }
        catch(IOException e)
        {
            System.out.println("AnteproyectoDAO: Error en la manipulacion del archivo anteproyectos.txt");
            respuesta = false;
        }  
        return respuesta;
    }
    
    public boolean GuardarDatosEvaluador(EvaluadorDTO objEvaluador)
    {
        boolean respuesta = false;
        File fichero = null;
        FileOutputStream fos;
        ObjectOutputStream ois;
        
        try
        {
            fichero = new File("evaluadores.txt");
            fos = new FileOutputStream(fichero);
            ois = new ObjectOutputStream(fos);
            ois.writeObject(objEvaluador);
            ois.close();
            respuesta = true;
        }
        catch(FileNotFoundException e)
        {
            System.out.println(" AnteproyectoDAO: Error en la localizacion del archivo anteproyectos.txt");
            respuesta = false;
        }
        catch(IOException e)
        {
            System.out.println("AnteproyectoDAO: Error en la manipulacion del archivo anteproyectos.txt");
            respuesta = false;
        }    
        return respuesta;
    }
    
    public ArrayList<AnteproyectoDTO> getFicheroAnteproyectos()
    {
        ArrayList<AnteproyectoDTO> ArrayAnteproyectos = new ArrayList<>();
        File fichero = new File("anteproyectos.txt");
        FileInputStream fis;
        ObjectInputStream ois;
        try
        {
            fis = new FileInputStream(fichero);
            ois = new ObjectInputStream(fis);
            ArrayAnteproyectos = (ArrayList<AnteproyectoDTO>) ois.readObject();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e)
        {
            System.out.println("Error en la manipulacion del archivo usuarios.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ArrayAnteproyectos;
    }    
    
    public boolean jefeModificarConcepto(int codigoAP, int conceptoAP)
    {
        boolean bandera = false;
        ArrayList<AnteproyectoDTO> ArrayAnteproyectos = this.getFicheroAnteproyectos();
        
        for(int i=0; i < ArrayAnteproyectos.size(); i++)
        {
            if(ArrayAnteproyectos.get(i).getCodigoAP() == codigoAP)
            {
                ArrayAnteproyectos.get(i).setConcepto(conceptoAP);
                bandera = true;
                break;
            }
        }
        return bandera;
    }

    public ArrayList<EvaluadorDTO> getFicheroEvaluadores()
    {
        ArrayList<EvaluadorDTO> ArrayEvaluadores = null;
        File fichero = new File("usuarios.txt");
        FileInputStream fis;
        ObjectInputStream ois;
        try
        {
            fis = new FileInputStream(fichero);
            ois = new ObjectInputStream(fis);
            ArrayEvaluadores= (ArrayList<EvaluadorDTO>) ois.readObject();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e)
        {
            System.out.println("Error en la manipulacion del archivo usuarios.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ArrayEvaluadores;
    }    
    
    public boolean EvaluadorModificarConcepto(ArrayList<EvaluadorDTO> ArrayInfoEvaluadores)
    {
        boolean bandera = false;
        
        return bandera;
    }
}
