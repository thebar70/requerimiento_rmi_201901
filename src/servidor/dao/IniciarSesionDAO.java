/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;

import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class IniciarSesionDAO {
    
    private ArrayList<UsuarioDTO> ArrayUsuarios;
    private UsuarioDTO administrador;
    
    public int getFicheroAdmin()
    {
        File fichero = new File("admin.txt");
        FileInputStream fis;
        ObjectInputStream ois;
        try
        {
            fis = new FileInputStream(fichero);
            ois = new ObjectInputStream(fis);
            administrador= (UsuarioDTO) ois.readObject();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error en la localizacion del archivo admin.txt");
        }
        catch(IOException e)
        {
            System.out.println("Error en la manipulacion del archivo admin.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public int getFicheroUsuarios()
    {
        File fichero = new File("usuarios.txt");
        FileInputStream fis;
        ObjectInputStream ois;
        try
        {
            fis = new FileInputStream(fichero);
            ois = new ObjectInputStream(fis);
            ArrayUsuarios= (ArrayList<UsuarioDTO>) ois.readObject();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e)
        {
            System.out.println("Error en la manipulacion del archivo usuarios.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public int getTipoUsuario(IniciarSesionDTO objIS)
    {
        int respuesta = -1;
        getFicheroAdmin();
        getFicheroUsuarios();
        System.out.println(":: "+administrador.getUsuario());
        System.out.println(":: "+administrador.getContrasena());
        if(administrador.getUsuario().equalsIgnoreCase(objIS.getUsuario()) && administrador.getContrasena().equals(objIS.getContrasena())){
            respuesta = administrador.getTipoUsuario();
        }else{
            for(int i=0; i < ArrayUsuarios.size(); i++)
            {
                if(ArrayUsuarios.get(i).getUsuario().equalsIgnoreCase(objIS.getUsuario()) && ArrayUsuarios.get(i).getContrasena().equals(objIS.getContrasena()))
                {
                    System.out.println("USUARIO CORRECTO");
                    respuesta = ArrayUsuarios.get(i).getTipoUsuario();
                    break;
                }
            }
        }
        return respuesta;
    }
}
