/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;

import servidor.dto.UsuarioDTO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author user
 */
public class UsuarioDAO {
    
    public void GuardarDatos(UsuarioDTO usuarios)
    {
        File fichero = null;
        FileOutputStream fos;
        ObjectOutputStream ois;
        
        try
        {
            fichero = new File("usuarios.txt");
            fos = new FileOutputStream(fichero);
            ois = new ObjectOutputStream(fos);
            ois.writeObject(usuarios);
            ois.close();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("UsuarioDAO: Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e)
        {
            System.out.println("UsuarioDAO: Error en la manipulacion del archivo usuarios.txt");
        }    
    }
    
}
