/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author user
 */
public class AnteproyectoDTO implements Serializable{
    private String modalidad;
    private String titulo;
    private int codigoAP;
    private String estudiente_1;
    private String estudainte_2;
    private String director;
    private String coDirector;
    private LocalDate fechaRegistro;
    private LocalDate fechaAprobacion;
    private int concepto;
    private int estado;
    private int numRevision;

    public AnteproyectoDTO(String modalidad, String titulo, int codigoAP, String estudiente_1, String estudainte_2, String director, String coDirector) {
        this.modalidad = modalidad;
        this.titulo = titulo;
        this.codigoAP = codigoAP;
        this.estudiente_1 = estudiente_1;
        this.estudainte_2 = estudainte_2;
        this.director = director;
        this.coDirector = coDirector;
        this.fechaRegistro = LocalDate.now();
        this.fechaAprobacion = null;
        this.concepto = 2;
        this.estado = 1;
        this.numRevision = 0;
    }

    public String getModalidad() {
        return modalidad;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getCodigoAP() {
        return codigoAP;
    }

    public String getEstudiente_1() {
        return estudiente_1;
    }

    public String getEstudainte_2() {
        return estudainte_2;
    }

    public String getDirector() {
        return director;
    }

    public String getCoDirector() {
        return coDirector;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public LocalDate getFechaAprobacion() {
        return fechaAprobacion;
    }

    public int getConcepto() {
        return concepto;
    }

    public int getEstado() {
        return estado;
    }

    public int getNumRevision() {
        return numRevision;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setCodigoAP(int codigoAP) {
        this.codigoAP = codigoAP;
    }

    public void setEstudiente_1(String estudiente_1) {
        this.estudiente_1 = estudiente_1;
    }

    public void setEstudainte_2(String estudainte_2) {
        this.estudainte_2 = estudainte_2;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setCoDirector(String coDirector) {
        this.coDirector = coDirector;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public void setFechaAprobacion(LocalDate fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public void setConcepto(int concepto) {
        this.concepto = concepto;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public void setNumRevision(int numRevision) {
        this.numRevision = numRevision;
    }
    
    
}
