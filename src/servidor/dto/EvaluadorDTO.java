/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author user
 */
public class EvaluadorDTO implements Serializable{
    private int codigoAp;
    private String nombreEvaluador_1;
    private int conceptoEval_1;
    private LocalDate fechaRevision_1;
    private String nombreEvaluador_2;
    private int conceptoEval_2;
    private LocalDate fechaRevision_2;

    public EvaluadorDTO(int codigoAp, String nombreEvaluador_1, String nombreEvaluador_2) {
        this.codigoAp = codigoAp;
        this.nombreEvaluador_1 = nombreEvaluador_1;
        this.conceptoEval_1 = 2;
        this.nombreEvaluador_2 = nombreEvaluador_2;
        this.conceptoEval_2 = 2;
    }

    public int getCodigoAp() {
        return codigoAp;
    }

    public String getNombreEvaluador_1() {
        return nombreEvaluador_1;
    }

    public int getConceptoEval_1() {
        return conceptoEval_1;
    }

    public LocalDate getFechaRevision_1() {
        return fechaRevision_1;
    }

    public String getNombreEvaluador_2() {
        return nombreEvaluador_2;
    }

    public int getConceptoEval_2() {
        return conceptoEval_2;
    }

    public LocalDate getFechaRevision_2() {
        return fechaRevision_2;
    }

    public void setCodigoAp(int codigoAp) {
        this.codigoAp = codigoAp;
    }

    public void setNombreEvaluador_1(String nombreEvaluador_1) {
        this.nombreEvaluador_1 = nombreEvaluador_1;
    }

    public void setConceptoEval_1(int conceptoEval_1) {
        this.conceptoEval_1 = conceptoEval_1;
    }

    public void setFechaRevision_1(LocalDate fechaRevision_1) {
        this.fechaRevision_1 = fechaRevision_1;
    }

    public void setNombreEvaluador_2(String nombreEvaluador_2) {
        this.nombreEvaluador_2 = nombreEvaluador_2;
    }

    public void setConceptoEval_2(int conceptoEval_2) {
        this.conceptoEval_2 = conceptoEval_2;
    }

    public void setFechaRevision_2(LocalDate fechaRevision_2) {
        this.fechaRevision_2 = fechaRevision_2;
    }
    
}
