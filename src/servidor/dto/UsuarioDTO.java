/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dto;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class UsuarioDTO implements Serializable{
    private String nombres;
    private int numIdentificacion;
    private String usuario;
    private String contrasena;
    private int tipoUsuario;

    public UsuarioDTO(String nombres, int numIdentificacion, String usuario, String contrasena, int tipoUser) {
        this.nombres = nombres;
        this.numIdentificacion = numIdentificacion;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.tipoUsuario = tipoUser;
    }

    public String getNombres() {
        return nombres;
    }

    public int getNumIdentificacion() {
        return numIdentificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setNumIdentificacion(int numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public void setTipoUsuario(int tipoUser) {
        this.tipoUsuario = tipoUser;
    }
    
}
