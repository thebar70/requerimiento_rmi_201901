/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.servidorObjetos;

import java.rmi.RemoteException;
import servidor.sop_rmi.GestionAnteproyectosImpl;
import servidor.sop_rmi.ClsGestionUsuariosImpl;

public class ServidorDeObjetos {
    
    public static void main(String args[]) throws RemoteException
    {
        int numPuertoRMIRegistry;
        String direccionIpRMIRegistry;
        
        //System.out.println("Cual es la direccion ip donde se encuentra  el rmiregistry ");
        direccionIpRMIRegistry = "localhost";
        //System.out.println("Cual es el numero de puerto por el cual escucha el rmiregistry ");
        numPuertoRMIRegistry = 2020; 
     
        ClsGestionUsuariosImpl objRemotoUser = new ClsGestionUsuariosImpl();
        GestionAnteproyectosImpl objRemotoAP = new GestionAnteproyectosImpl();
        
        try
        {
           servidor.utilidades.UtilidadesRegistroS.arrancarNS(numPuertoRMIRegistry);
           servidor.utilidades.UtilidadesRegistroS.RegistrarObjetoRemoto(objRemotoUser, direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoUsuarios");            
           
           servidor.utilidades.UtilidadesRegistroS.RegistrarObjetoRemoto(objRemotoAP, direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoAnteproyectos");            
        } catch (Exception e)
        {
            System.err.println("No fue posible Arrancar el NS o Registrar el objeto remoto" +  e.getMessage());
        }
        
        
    }
}
