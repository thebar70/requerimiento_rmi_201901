/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.rmi.RemoteException;


public class CallbackImpl implements CallbackInt {

    @Override
    public String notificarEvaluador(String nombreEval, int codigoAP) throws RemoteException {
        String mensaje = ("Mensaje del servidor, evaluador asignado: "+ nombreEval + ", Codigo del anteproyecto: "+ codigoAP);
        return mensaje;
    }
    
}
