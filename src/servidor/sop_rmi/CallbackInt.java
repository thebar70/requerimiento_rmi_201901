/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 *
 * @author user
 */
public interface CallbackInt extends Remote{
    public String notificarEvaluador(String nombreEval, int codigoAP) throws RemoteException;   
}
