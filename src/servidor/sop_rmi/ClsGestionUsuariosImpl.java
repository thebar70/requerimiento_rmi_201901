/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import servidor.dao.UsuarioDAO;
import servidor.dao.IniciarSesionDAO;


public class ClsGestionUsuariosImpl extends UnicastRemoteObject implements GestionUsuariosInt {
    private UsuarioDAO infoUsuario;
    private IniciarSesionDAO infoInicioSesion;

    public ClsGestionUsuariosImpl() throws RemoteException{
        super();
        infoUsuario = new UsuarioDAO();
        infoInicioSesion = new IniciarSesionDAO();
    }

    @Override
    public int iniciarSesion(IniciarSesionDTO objSesion)throws RemoteException{
        int tipoUsuario = infoInicioSesion.getTipoUsuario(objSesion);
        if(tipoUsuario == -1)
        {
            System.out.println("No existe");
        }
        return tipoUsuario;
    }

    @Override
    public boolean registrarUsuario(UsuarioDTO objUsuario)throws RemoteException{
        boolean resultado = false;
        infoUsuario.GuardarDatos(objUsuario);
        return resultado;
    }
    
}
