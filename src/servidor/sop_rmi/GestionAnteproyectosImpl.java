/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import servidor.dto.AnteproyectoDTO;
import servidor.dao.AnteproyectoDAO;
import servidor.dto.EvaluadorDTO;


public class GestionAnteproyectosImpl extends UnicastRemoteObject implements GestionAnteproyectosInt {
    private AnteproyectoDAO objAPDAO = new AnteproyectoDAO();

    public GestionAnteproyectosImpl() throws RemoteException{
        super();
        objAPDAO = new AnteproyectoDAO();
    }

    @Override
    public boolean registrarAnteproyectos(AnteproyectoDTO objAnteproyecto) throws RemoteException {
        return objAPDAO.GuardarDatosAP(objAnteproyecto);
    }

    @Override
    public boolean asignarEvaluadores(EvaluadorDTO objEvaluador)throws RemoteException{
        return objAPDAO.GuardarDatosEvaluador(objEvaluador);
    }

    @Override
    public AnteproyectoDTO buscarAnteproyecto(int codigoAP) throws RemoteException {
       AnteproyectoDTO AnteproyectoAux = null;
        ArrayList<AnteproyectoDTO> ArrayAnteproyectos = objAPDAO.getFicheroAnteproyectos();
        
        for (int i = 0; i< ArrayAnteproyectos.size(); i++){
            if(ArrayAnteproyectos.get(i).getCodigoAP() == codigoAP){
                AnteproyectoAux = ArrayAnteproyectos.get(i);
                break;
            }
        }
        return AnteproyectoAux;
    }

    @Override
    public ArrayList<AnteproyectoDTO> listarAnteproyectos() throws RemoteException {
        return objAPDAO.getFicheroAnteproyectos();
    }

    @Override
    public boolean modificarConcepto(int codigoAP,String nombreEval ,int conceptoAP) throws RemoteException {
        return true;
//        ArrayList<EvaluadorDTO> ArrayInfoEvaluadores = objAPDAO.getFicheroEvaluadores();
//        
//        for(int i=0; i < ArrayInfoEvaluadores.size(); i++)
//        {
//            if(ArrayInfoEvaluadores.get(i).getCodigoAp()== codigoAP)
//            {
//                if(ArrayInfoEvaluadores.get(i).getNombreEvaluador_1().equalsIgnoreCase(nombreEval)){
//                    
//                }
//                ArrayInfoEvaluadores.get(i).setConceptoEval_1(conceptoAP);
//                bandera = true;
//                break;
//            }
//        }

    }
    
}
