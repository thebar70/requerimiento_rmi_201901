/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author user
 */
public interface GestionUsuariosInt extends Remote{
    int iniciarSesion(IniciarSesionDTO objSesion) throws RemoteException;
    boolean registrarUsuario(UsuarioDTO objUsuario)throws RemoteException;
}
